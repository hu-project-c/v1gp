import os

from dotenv import load_dotenv
from flask import Flask
from flask_restful import Api, Resource
from pymongo import MongoClient
from recommendations.simple_recommendation import recommedations
from recommendations.quarterly_recommendation import quarterly_recommendation
from recommendations.bundles_recommendations import bundles_recommendation
from recommendations.gender_recommendation import gender_recommendation
from recommendations.price_recommendation import price_recommendation
from recommendations.anderen_kochten_ook_recommendation import ako_recommendation
from datetime import datetime, timedelta

app = Flask(__name__)
api = Api(app)

# We define these variables to (optionally) connect to an external MongoDB
# instance.
envvals = ["MONGODBUSER", "MONGODBPASSWORD", "MONGODBSERVER"]
dbstring = 'mongodb+srv://{0}:{1}@{2}/test?retryWrites=true&w=majority'

# Since we are asked to pass a class rather than an instance of the class to the
# add_resource method, we open the connection to the database outside of the 
# Recom class.
load_dotenv()
if os.getenv(envvals[0]) is not None:
    envvals = list(map(lambda x: str(os.getenv(x)), envvals))
    client = MongoClient(dbstring.format(*envvals))
else:
    client = MongoClient()
database = client.huwebshop


class Recom(Resource):
    """ This class represents the REST API that provides the recommendations for
    the webshop. At the moment, the API simply returns a random set of products
    to recommend."""

    def get(self, profileid, count):
        """ This function represents the handler for GET requests coming in
        through the API. It currently returns the top count products.
        :param profileid:
        :param count:
        :return list,code:
        """
        recommendation_engine = recommedations.SimpleRecommendation(False)
        prod_ids = recommendation_engine.get_recommedations(count)
        print(prod_ids)
        return prod_ids, 200

class QuarterRecom(Resource):
    def get(self):
        obj = quarterly_recommendation.QuarterlyRecommendation(False)
        today = datetime.now()

        quarter = obj.get_quarter(today)
        products = obj.get_recom(quarter)

        return products

class BundleRecom(Resource):
    def get(self, profileid):
        obj = bundles_recommendation.BundlesRecommendation(False)
        return obj.get_bundles_recommendation(profileid)


class GenderRecom(Resource):
    def get(self, profileid):
        obj = gender_recommendation.GenderRecommendation(False)
        return obj.get_gender_recommendation(profileid)

class AkoRecom(Resource):
    def get(self, profileid):
        obj = ako_recommendation.AkoRecommendation(False)
        return obj.get_ako_recommendation(profileid)

class PriceRecom(Resource):
    def get(self, profileid):
        obj = price_recommendation.PriceRecommendation(False)
        return obj.get_price_recommendation(profileid)

# This method binds the Recom class to the REST API, to parse specifically
# requests in the format described below.
api.add_resource(Recom, "/<string:profileid>/<int:count>")
api.add_resource(QuarterRecom, "/quarter")
api.add_resource(BundleRecom, "/bundle/<string:profileid>")
api.add_resource(GenderRecom, "/gender/<string:profileid>")
api.add_resource(AkoRecom, "/ako/<string:profileid>")
api.add_resource(PriceRecom, "/price/<string:profileid>")
