from recommendations import postgres_handler


class SimpleRecommendation:
    def __init__(self, bool_all):
        self.database = postgres_handler.Database()

        if bool_all:
            self.create_total_sold_products_table()  # calling createTable function from class
            self.insert_total_sold_products()  # calling insertTable function from class

    def create_total_sold_products_table(self):
        """
        Creates huengine_total_sold_products table from sql file
        :return void:
        """
        try:
            with open('simple_recommendation/sql/huengine_total_sold_products.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass

    def insert_total_sold_products(self):
        """
        Inserts data into huengine_total_sold_products table with select statement.
        :return void:
        """
        sql = "INSERT INTO huengine_total_sold_products (total_sold, product_id, category, sub_category)  " \
              "SELECT sum(product_amount) as total_sold, product_id, category, sub_category " \
              "FROM huengine_orders_products_count LEFT JOIN huengine_product hp on huengine_orders_products_count.product_id = hp.id " \
              "GROUP BY product_id, category, sub_category ORDER BY total_sold DESC;"

        self.database.post(sql)

    def get_recommedations(self, count):
        """
        Get most sold products unique by category and limit 4
        :return list[product_id]:
        """
        sql = "SELECT product_id, category FROM huengine_total_sold_products;"
        recommendations = {'products': [], 'categories': []}

        products = self.database.get(sql)

        for product in products:
            if product[1] not in recommendations['categories']:
                recommendations['categories'].append(product[1])
                recommendations['products'].append(product[0])
            if len(recommendations['products']) == int(count):
                return recommendations['products']
