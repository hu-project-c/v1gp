DROP table IF EXISTS huengine_total_sold_products;

create table huengine_total_sold_products
(
    id           serial  not null
        constraint huengine_total_sold_products_pk
            primary key,
    total_sold   varchar not null,
    product_id   varchar not null
        constraint huengine_total_sold_products_huengine_product_id_fk
            references huengine_product,
    category     varchar,
    sub_category varchar
);

alter table huengine_total_sold_products
    owner to postgres;