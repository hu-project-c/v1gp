DROP TABLE IF EXISTS huengine_bundles_profile;

create table huengine_bundles_profile
(
    id         serial  not null
        constraint huengine_bundles_profile_pk
            primary key,
    session_id varchar not null
        constraint huengine_bundles_profile_huengine_session_id_fk
            references huengine_session,
    profile_id varchar not null
        constraint huengine_bundles_profile_huengine_profile_id_fk
            references huengine_profile,
    order_id   varchar not null
        constraint huengine_bundles_profile_huengine_order_id_fk
            references huengine_order,
    product_id varchar not null
        constraint huengine_bundles_profile_huengine_product_id_fk
            references huengine_product,
    bundle     varchar not null
);

alter table huengine_bundles_profile
    owner to postgres;

create unique index huengine_bundles_profile_id_uindex
    on huengine_bundles_profile (id);

