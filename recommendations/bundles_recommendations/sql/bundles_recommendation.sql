DROP TABLE IF EXISTS huengine_bundles_recommendation;

create table huengine_bundles_recommendation
(
    id         serial  not null
        constraint huengine_bundles_recommendation_pk
            primary key,
    profile_id varchar not null
        constraint huengine_bundles_recommendation_huengine_profile_id_fk
            references huengine_profile,
    product1   varchar not null,
    product2   varchar not null,
    product3   varchar not null,
    product4   varchar not null
);

alter table huengine_bundles_recommendation
    owner to postgres;

create unique index huengine_bundles_recommendation_id_uindex
    on huengine_bundles_recommendation (id);

