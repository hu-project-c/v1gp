from recommendations import postgres_handler
from collections import Counter


class BundlesRecommendation:
    def __init__(self, bool_all):
        self.database = postgres_handler.Database()
        if bool_all:
            self.create_bundles_profile_table()
            self.insert_bundles_profile_table()

            self.create_bundles_recommendation_table()
            data = self.get_recommendations_for_profiles()
            self.insert_bundles_recommendation(data)

    def create_bundles_profile_table(self):
        """
        Creates huengine_bundles_profile table from sql file
        :return void:
        """
        try:
            with open('bundles_recommendations/sql/bundles_profile.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass


    def create_bundles_recommendation_table(self):
        """
        Creates huengine_bundles_recommendation table from sql file
        :return void:
        """
        try:
            with open('bundles_recommendations/sql/bundles_recommendation.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass

    def insert_bundles_profile_table(self):
        """
        Inserts data into huengine_bundles_profile table with select statement.
        :return void:
        """
        sql = "INSERT INTO huengine_bundles_profile (session_id, profile_id, order_id, product_id, bundle) " \
              "SELECT huengine_profile_sessions.session_id, huengine_profile_sessions.profile_id, " \
              "huengine_orders_products_count.order_id, huengine_property.product_id, huengine_property.value " \
              "FROM huengine_order " \
              "INNER JOIN huengine_profile_sessions ON huengine_order.session_id = huengine_profile_sessions.session_id " \
              "INNER JOIN huengine_orders_products_count ON huengine_order.id = huengine_orders_products_count.order_id " \
              "INNER JOIN huengine_property ON huengine_orders_products_count.product_id = huengine_property.product_id " \
              "WHERE huengine_property.name = 'discount' AND huengine_property.value IS NOT NULL"

        self.database.post(sql)

    def get_recommendations_for_profiles(self):
        """
        This function request all unique profiles and gets all the bought products for the profiles.
        For every product is checked if the product has a property discount.
        If thats true, the function will count the most products with the same discount and returns the discount with the bought products ids and the profile id.
        :return data: tuple(profile_id, bundle, product_ids)
        """
        sql_profiles = "SELECT DISTINCT profile_id FROM huengine_bundles_profile"
        profile_ids = self.database.get(sql_profiles)

        data = []

        for profile in profile_ids:
            sql_bundles = f"SELECT product_id, bundle FROM huengine_bundles_profile WHERE profile_id = '{profile[0]}'"
            bundles = self.database.get(sql_bundles)

            freq = Counter(i[1] for i in bundles).most_common(1)[0][0]
            already_bought = []

            for bundle in bundles:
                if bundle[1] == freq:
                    already_bought.append(bundle[0])
            data.append((profile[0], freq, already_bought))

        return data


    def insert_bundles_recommendation(self, data):
        """
        This function requests 4 product_ids with a specific value and where the product id is not equel to the already bought products by the profile.
        Then the 4 product ids are inserted in the huengine_bundles_recommendation table with the profile id.
        :return void:
        """
        for i in data:
            sql_products = "SELECT huengine_property.product_id FROM huengine_property " \
                  "WHERE huengine_property.name = 'discount' AND " \
                  "huengine_property.value IS NOT NULL AND " \
                  f"huengine_property.value = '{i[1]}' AND " \
                  f"huengine_property.product_id != '{', '.join(i[2])}' LIMIT 4;"


            products = self.database.get(sql_products)


            if len(products) == 4 and "'" not in products[0][0] and "'" not in products[1][0] and "'"not in products[2][0] and "'" not in products[3][0]:
                sql_insert = "INSERT INTO huengine_bundles_recommendation (profile_id, product1, product2, product3, product4) " \
                                f"VALUES ('{i[0]}', '{products[0][0]}', '{products[1][0]}', '{products[2][0]}', '{products[3][0]}')"


                self.database.post(sql_insert)


    def get_bundles_recommendation(self, profile_id):
        sql = f"SELECT product1, product2, product3, product4 FROM huengine_bundles_recommendation WHERE profile_id = '{profile_id}'"
        product_ids = self.database.get(sql)
        return product_ids
