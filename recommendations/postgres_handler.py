import psycopg2
import os
from dotenv import load_dotenv

class Database:
    def __init__(self):
        """
        Load and set database credentials from the .env
        :return void:
        """
        load_dotenv()
        self.database = os.environ.get('DATABASE')
        self.user = os.environ.get('DBUSER')
        self.password = os.environ.get('PASSWORD')
        self.port = os.environ.get('PORT')
        self.conn = None
        self.cur = None

    def connect(self):
        """
        Opens connection with database and sets cursor
        :return void:
        """
        self.conn = psycopg2.connect(f"dbname={self.database} user={self.user} password={self.password} port={self.port}")
        self.cur = self.conn.cursor()

    def post(self, query):
        """
        Executes a 'post' query
        :param query:
        :return void:
        """
        # Connect
        self.connect()

        # Execute query
        self.cur.execute(query)
        self.conn.commit()

        # Close connection
        self.close()

    def get(self, query):
        """
        Executes a 'get' query
        :param query:
        :return void:
        """
        try:
            # Connect
            self.connect()

            # Execute query
            self.cur.execute(query)

            results = self.cur.fetchall()

            # Close connection
            self.close()

            return results
        except psycopg2.Error:
            print(f"Skipped due to syntax error: {query}")

    def close(self):
        """
        Closes connection with database
        :return void:
        """
        self.cur.close()
        self.conn.close()
