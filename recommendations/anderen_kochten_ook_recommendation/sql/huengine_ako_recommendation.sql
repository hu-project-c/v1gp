DROP TABLE IF EXISTS huengine_ako_recommendation;

create table huengine_ako_recommendation
(
    id         serial  not null
        constraint huengine_ako_recommendation_pk
            primary key,
    profile_id varchar not null
        constraint huengine_ako_recommendation_huengine_profile_id_fk
            references huengine_profile,
    product1   varchar,
    product2   varchar,
    product3   varchar,
    product4   varchar
);

alter table huengine_ako_recommendation
    owner to postgres;

create unique index huengine_ako_recommendation_id_uindex
    on huengine_ako_recommendation (id);

