from recommendations import postgres_handler


class AkoRecommendation:
    def __init__(self, bool_all):
        self.database = postgres_handler.Database()
        if bool_all:
            self.create_ako_recommendation_table()
            self.getAllOrders()
            self.getOtherOrders()
            self.insertRecommendation()


    def create_ako_recommendation_table(self):
        """
        Creates huengine_ako_recommendation table from sql file
        :return void:
        """
        try:
            with open('anderen_kochten_ook_recommendation/sql/huengine_ako_recommendation.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass

    def getAllOrders(self):
        """
        Method to get all orders from the database. return type = tuple(order_id, profile_id, product_id_
        :return void:
        """
        sql ="SELECT order_id, profile_id, product_id FROM huengine_order " \
             "INNER JOIN huengine_profile_sessions ON huengine_profile_sessions.session_id = huengine_order.session_id " \
             "INNER JOIN huengine_orders_products_count ON huengine_order.id = huengine_orders_products_count.order_id"

        data = self.database.get(sql)

        order_ids = []
        self.orders = []
        for row in data:
            if row[0] not in order_ids:
                order_ids.append(row[0])
                self.orders.append(row)


    def getOtherOrders(self):
        """
        Loops through orders and gets other orders with the same bought product.
        Then for every other order it will get they're bought products and add them to the recoomendation variable.
        :return void:
        """
        self.recommendations = []

        for row in self.orders:
            product_ids = []

            sql_orders = f"SELECT order_id FROM huengine_orders_products_count WHERE order_id != '{row[0]}' " \
                         f"AND product_id = '{row[2]}'"
            other_orders = self.database.get(sql_orders)


            for order_id in other_orders:
                sql_products = f"SELECT product_id FROM huengine_orders_products_count WHERE order_id = '{order_id[0]}' " \
                               f"AND product_id != '{row[2]}' LIMIT 1"
                data = self.database.get(sql_products)

                if len(product_ids) == 4:
                    break
                else:
                    try:
                        product_ids.append(data[0][0])
                    except:
                        pass

            self.recommendations.append({row[1]: product_ids})


    def insertRecommendation(self):
        """
        Insert huengine_ako_recommendation table with profile id's and recommended products for that profile.
        :return void:
        """
        for row in self.recommendations:

            profile_id = list(row.keys())[0]
            products = list(row.values())[0]

            new_list = products.copy()
            list_len = len(products)

            if list_len != 4:
                continue

            new_list.insert(0, profile_id)


            sql = f"INSERT INTO huengine_ako_recommendation (profile_id, product1, product2, product3, product4) " \
                  f"VALUES {tuple(new_list)}"
            self.database.post(sql)

    def get_ako_recommendation(self, profile_id):
        sql = f"SELECT product1, product2, product3, product4 FROM huengine_ako_recommendation WHERE profile_id = '{profile_id}'"
        product_ids = self.database.get(sql)
        return product_ids
