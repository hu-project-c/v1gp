DROP TABLE IF EXISTS huengine_price_profile;

create table huengine_price_profile
(
    id         serial  not null
        constraint huengine_price_profile_pk
            primary key,
    session_id varchar not null
        constraint huengine_bundles_profile_huengine_session_id_fk
            references huengine_session,
    profile_id varchar not null
        constraint huengine_price_profile_huengine_profile_id_fk
            references huengine_profile,
    order_id   varchar not null
        constraint huengine_bundles_profile_huengine_order_id_fk
            references huengine_order,
    price_id varchar not null
        constraint huengine_price_profile_huengine_price_id_fk
            references huengine_price,
    selling_price float not null,
    product_price_id varchar not null
        constraint huengine_bundles_profile_huengine_price_id_fk
            references huengine_price
);

alter table huengine_price_profile
    owner to postgres;

create unique index huengine_price_profile_id_uindex
    on huengine_price_profile (id);
