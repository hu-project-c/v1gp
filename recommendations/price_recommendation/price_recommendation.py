from recommendations import postgres_handler

class PriceRecommendation:

    def __init__(self, bool_all):
        self.database = postgres_handler.Database()
        if bool_all:
            self.create_price_profile_table()
            self.create_price_recommendation_table()
            self.insert_price_profile_table()
            self.get_average_spending()

    def create_price_profile_table(self):
        """
        Creates huengine_price_profile table from sql file
        :return void:
        """
        try:
            with open('price_recommendation/sql/price_profile.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass

    def create_price_recommendation_table(self):
        """
        Creates huengine_price_recommendation table from sql file
        :return void:
        """
        with open('price_recommendation/sql/price_recommendation.sql', 'r') as sql:
            self.database.post(sql.read())

    def insert_price_profile_table(self):
        """
        Inserts data into huengine_price_profile table with select statement.
        :return void:
        """
        sql = "INSERT INTO huengine_price_profile (session_id, profile_id, order_id, price_id, selling_price, product_price_id) " \
              "SELECT huengine_profile_sessions.session_id, huengine_profile_sessions.profile_id, " \
              "huengine_orders_products_count.order_id, huengine_price.id, huengine_price.selling_price, " \
              "huengine_product.price_id " \
              "FROM huengine_order " \
              "INNER JOIN huengine_profile_sessions ON huengine_order.session_id = huengine_profile_sessions.session_id " \
              "INNER JOIN huengine_orders_products_count ON huengine_order.id = huengine_orders_products_count.order_id " \
              "INNER JOIN huengine_product ON huengine_orders_products_count.product_id = huengine_product.id " \
              "INNER JOIN huengine_price ON huengine_product.price_id = huengine_price.id"

        self.database.post(sql)

    def get_average_spending(self):
        """
        Calculate the average spending of a profile and recommended products that lay in the same price ranges
        """

        sql_profiles = "SELECT DISTINCT profile_id FROM huengine_price_profile"
        profile_ids = self.database.get(sql_profiles)

        for profile_id in profile_ids:
            sql_prices = f"SELECT selling_price FROM huengine_price_profile WHERE profile_id = '{profile_id[0]}'"
            prices = self.database.get(sql_prices)

            avg_price = sum(map(sum, prices)) / len(prices)
            self.insert_price_recommendation(profile_id, avg_price)

    def insert_price_recommendation(self, profid, avg_price):
        """
        Find products which lay in the same price range and insert them into the price_recommendation table
        """
        min_price_range = round(avg_price * 0.99)
        max_price_range = round(avg_price * 1.01)

        sql_products = "SELECT DISTINCT huengine_product.price_id, huengine_product.id, huengine_price.id, huengine_price.selling_price " \
                       "FROM huengine_product " \
                       "INNER JOIN huengine_price ON huengine_product.price_id = huengine_price.id " \
                       f"WHERE huengine_price.selling_price >= '{min_price_range}' AND huengine_price.selling_price <= '{max_price_range}' " \
                       "LIMIT 4"

        products = self.database.get(sql_products)

        if len(products) == 4:
            insert_sql_products = "INSERT INTO huengine_price_recommendation (profile_id, product1, product2, product3, product4)" \
                                  f"VALUES ('{profid[0]}', '{products[0][1]}', '{products[1][1]}', '{products[2][1]}', '{products[3][1]}')"


            self.database.post(insert_sql_products)

    def get_price_recommendation(self, profile_id):
        sql = f"SELECT product1, product2, product3, product4 FROM huengine_price_recommendation WHERE profile_id = '{profile_id}'"
        product_ids = self.database.get(sql)
        return product_ids
