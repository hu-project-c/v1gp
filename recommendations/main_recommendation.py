from recommendations.anderen_kochten_ook_recommendation import ako_recommendation
from recommendations.bundles_recommendations import bundles_recommendation
from recommendations.quarterly_recommendation import quarterly_recommendation
from recommendations.simple_recommendation import recommedations
from recommendations.gender_recommendation import gender_recommendation
from recommendations.price_recommendation import price_recommendation

if __name__ == '__main__':
    ako = ako_recommendation.AkoRecommendation(True)
    bundle = bundles_recommendation.BundlesRecommendation(True)
    qr = quarterly_recommendation.QuarterlyRecommendation(True)
    simple = recommedations.SimpleRecommendation(True)
    gender = gender_recommendation.GenderRecommendation(True)
    price = price_recommendation.PriceRecommendation(True)