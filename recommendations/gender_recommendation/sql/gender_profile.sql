DROP TABLE IF EXISTS huengine_gender_profile;

create table huengine_gender_profile
(
    id         serial  not null
        constraint huengine_gender_profile_pk
            primary key,
    session_id varchar not null
        constraint huengine_gender_profile_huengine_session_id_fk
            references huengine_session,
    profile_id varchar not null
        constraint huengine_gender_profile_huengine_profile_id_fk
            references huengine_profile,
    order_id   varchar not null
        constraint huengine_gender_profile_huengine_order_id_fk
            references huengine_order,
    product_id varchar not null
        constraint huengine_gender_profile_huengine_product_id_fk
            references huengine_product,
    gender    varchar not null
);

alter table huengine_gender_profile
    owner to postgres;

create unique index huengine_gender_profile_id_uindex
    on huengine_gender_profile (id);