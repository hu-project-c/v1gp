from recommendations import postgres_handler


class GenderRecommendation:

    def __init__(self, bool_all):
        self.database = postgres_handler.Database()
        if bool_all:
            self.create_gender_profile_table()
            self.create_gender_recommendation_table()

            self.insert_gender_profile_table()
            self.create_gender_recommendation()

    def create_gender_profile_table(self):
        """
        Creates huengine_gender_profile table from sql file
        :return void:
        """
        try:
            with open('gender_recommendation/sql/gender_profile.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass

    def create_gender_recommendation_table(self):
        """
        Creates huengine_gender_recommendation table from sql file
        :return void:
        """
        try:
            with open('gender_recommendation/sql/gender_recommendation.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass

    def insert_gender_profile_table(self):
        """
        Inserts data into huengine_gender_profile table with select statement.
        """
        sql = "INSERT INTO huengine_gender_profile(session_id, profile_id, order_id, product_id, gender) " \
              "SELECT huengine_profile_sessions.session_id, huengine_profile_sessions.profile_id, " \
              "huengine_orders_products_count.order_id, huengine_property.product_id, huengine_property.value " \
              "FROM huengine_order " \
              "INNER JOIN huengine_profile_sessions ON huengine_order.session_id = huengine_profile_sessions.session_id " \
              "INNER JOIN huengine_orders_products_count ON huengine_order.id = huengine_orders_products_count.order_id " \
              "INNER JOIN huengine_property ON huengine_orders_products_count.product_id = huengine_property.product_id " \
              "WHERE huengine_property.name = 'doelgroep' AND huengine_property.value = 'Mannen' OR huengine_property.value = 'Vrouwen'"

        self.database.post(sql)

    def create_gender_recommendation(self):
        """
        Find out which gender products profiles have bought the most
        :return void:
        """

        sql_profiles = "SELECT DISTINCT profile_id FROM huengine_gender_profile"
        profile_ids = self.database.get(sql_profiles)

        for profile_id in profile_ids:

            # Keep track of how many woman/men products a profile has bought
            bought_men_product = 0
            bought_women_product = 0

            sql_genders = f"SELECT gender FROM huengine_gender_profile WHERE profile_id = '{profile_id[0]}'"
            genders = self.database.get(sql_genders)

            for gender in genders:
                if gender[0] == 'Mannen':
                    bought_men_product += 1
                elif gender[0] == 'Vrouwen':
                    bought_women_product += 1

            if bought_men_product > bought_women_product:
                # If a profile has only bought male product(s)
                if bought_men_product - bought_women_product >= bought_men_product:
                    self.insert_gender_recommendation('Mannen', profile_id)
            elif bought_women_product > bought_men_product:
                # If a profile has only bought women product(s)
                if bought_women_product - bought_men_product >= bought_women_product:
                    self.insert_gender_recommendation('Vrouwen', profile_id)

    def insert_gender_recommendation(self, preffered_gender, profid):
        """
        Insert the gender recemmendation into the table
        """
        sql_products = "SELECT huengine_product.id, huengine_property.product_id, huengine_property.value " \
                       "FROM huengine_product " \
                       "INNER JOIN huengine_property ON huengine_product.id = huengine_property.product_id " \
                       f"WHERE huengine_property.value = '{preffered_gender}' " \
                       "ORDER BY RANDOM () " \
                       "LIMIT 4 "

        products = self.database.get(sql_products)

        if len(products) == 4:

            for product in products:
                if "'" in product[1]:
                    return

            insert_sql_products = "INSERT INTO huengine_gender_recommendation (profile_id, product1, product2, product3, product4)" \
                                  f"VALUES ('{profid[0]}', '{products[0][1]}', '{products[1][1]}', '{products[2][1]}', '{products[3][1]}')"

            self.database.post(insert_sql_products)

    def get_gender_recommendation(self, profile_id):
        sql = f"SELECT product1, product2, product3, product4 FROM huengine_gender_recommendation WHERE profile_id = '{profile_id}'"
        product_ids = self.database.get(sql)
        return product_ids
