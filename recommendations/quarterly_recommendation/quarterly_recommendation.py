from recommendations import postgres_handler
from operator import itemgetter
import math

class QuarterlyRecommendation():
    def __init__(self, bool_all):
        self.database = postgres_handler.Database()
        if bool_all:
            self.get_orders()

    def create_quarterly_table(self):
        """
        Create table for the recommendation
        """
        try:
            with open('quarterly_recommendation/sql/quarterly_recommendation.sql', 'r') as sql:
                self.database.post(sql.read())
        except:
            pass


    def get_orders(self):
        """
        Get all orders from database
        """

        self.create_quarterly_table()

        # Get orders from database
        sql = "SELECT DISTINCT huengine_order.id as order_id, huengine_session.id as session_id, " \
              "huengine_session.session_end, huengine_orders_products_count.product_id, " \
              "huengine_orders_products_count.product_amount " \
              "FROM huengine_order " \
              "INNER JOIN huengine_session ON huengine_order.session_id = huengine_session.id " \
              "INNER JOIN huengine_orders_products_count ON huengine_orders_products_count.order_id = huengine_order.id " \
              "ORDER BY product_amount desc"

        orders = self.database.get(sql)
        self.order_orders(orders)

    def order_orders(self, orders):
        """
        Orders the orders in quarters
        """

        quarter_dict = {
            "Q1": [

            ],
            "Q2": [

            ],
            "Q3": [

            ],
            "Q4": [

            ]
        }

        id_list = []
        # Categorize orders
        if orders:
            for order in orders:
                order_quarter = self.get_quarter(order[2])
                list_to_append = [order[-2], order[-1]]
                quarter_dict[order_quarter].append(list_to_append[0])
                id_list.append(list_to_append[0])

        # Products with quarter and amount sold
        product_quarter_amount = {}

        # Add products per quarter to new dict
        for key in quarter_dict:
            temp_list = []
            for value in quarter_dict[key]:
                if value not in temp_list:
                    # Amount a value is in the current quarter
                    amount_current_quarter = quarter_dict[key].count(value)

                    # Values to append
                    values = [key, amount_current_quarter]

                    # Check if the key in dict is already set
                    if value not in product_quarter_amount:
                        product_quarter_amount[value] = [values]
                    else:
                        product_quarter_amount[value].append(values)
                temp_list.append(value)

        # Get percentage per product and insert.
        self.get_percentage_insert(product_quarter_amount)

    def get_percentage_insert(self, product_dict):
        """
        Calculate percentage per product and insert best ones
        """

        quarter_dict = {
            "Q1": [

            ],
            "Q2": [

            ],
            "Q3": [

            ],
            "Q4": [

            ]
        }

        # Loop through product dict
        for key in product_dict:
            # Sum
            whole = 0

            # Calc total sum
            for value in product_dict[key]:
                whole+= value[1]

            # Calc percentage
            for value in product_dict[key]:
                percentage = self.calc_percentage(value[1], whole)
                if percentage != 100:
                    quarter_dict[value[0]].append([key, percentage])

        for key in quarter_dict:
            sorted_list = sorted(quarter_dict[key], key=itemgetter(1))

            if sorted_list:
                if len(sorted_list) > 4:
                    to_insert = [key, sorted_list[-1], sorted_list[-2], sorted_list[-3], sorted_list[-4]]
                    self.insert_data(to_insert)

    def calc_percentage(self, part, whole):
        """
        Calculate percentage
        :return: float
        """
        return (part / whole * 100)

    def insert_data(self, data):
        """
        Insert data into huengine_quarterly_recommendation
        """
        sql = f"INSERT INTO huengine_quarterly_recommendation (quarter, id1, id2, id3, id4) VALUES('{data[0]}', '{data[1][0]}', '{data[2][0]}', '{data[3][0]}', '{data[4][0]}')"
        self.database.post(sql)

    def get_recom(self, quarter):
        """
        Get the recommendation
        """
        sql = f"SELECT id1, id2, id3, id4 FROM huengine_quarterly_recommendation WHERE quarter like '{quarter}'"
        result = self.database.get(sql)
        return result


    def get_quarter(self, d):
        """
        Calculate quarter
        """

        return "Q%d" % (math.ceil(d.month / 3))
