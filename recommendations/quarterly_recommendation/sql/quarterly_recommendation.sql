DROP TABLE IF EXISTS huengine_quarterly_recommendation;

create table huengine_quarterly_recommendation
(
    id      serial  not null
        constraint huengine_quarterly_recommendation_pk
            primary key,
    quarter varchar not null,
    id1     varchar not null,
    id2     varchar not null,
    id3     varchar not null,
    id4     varchar not null
);

alter table huengine_quarterly_recommendation
    owner to postgres;

create unique index huengine_quarterly_recommendation_id_uindex
    on huengine_quarterly_recommendation (id);